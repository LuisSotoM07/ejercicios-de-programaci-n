﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tabla_del_7
{
    class Program
    {
        static void Main(string[] args)
        {
            int c = 1;
            int t = 7;
            int r = 0;

            while (c <= 12)
            {
                r = c * t;

                Console.WriteLine(c + " X " + t + " = " + r);

                c = c + 1;
            }
            Console.ReadKey();
        }
    }
}
