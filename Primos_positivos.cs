﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Primos_Positivos
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 2, divisible=0;
            while (num<=100)
            {
                for (int i = 1; i < num; i++)
                {
                    if (num % i == 0)
                    {
                        divisible++;
                    }
                    if (divisible > 2)
                    {
                        break;
                    }
                }
                if (divisible == 2)
                {
                    Console.WriteLine("{0} es primo", num);
                }
                divisible = 0;
                num++;
            }
            Console.ReadKey();
        }
    }
}
