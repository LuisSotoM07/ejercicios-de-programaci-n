﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contador_de_Digitos
{
    class Program
    {
        static void Main(string[] args)
        {
            int num, dig = 0;
            Console.WriteLine("Digite un numero: ");
            num = Convert.ToInt32(Console.ReadLine());

            if (num <= 0)
            {
                Console.WriteLine("Digite un numero Random");
            }
            else
            {
                do
                {
                    num = num / 10;
                    dig = dig + 1;

                } while (num > 0);
                Console.WriteLine("Tiene " + dig);
                Console.ReadLine();
            }
            Console.ReadKey();
        }
    }
}
